# README #

Just run the script

### What is this repository for? ###

Holding code until someone wants it

### How do I get set up? ###

All of the modules used should be part of the base install of python 2.7. 
To use it, just run it against a classes.dex file with --strings as shown below and it will output a list of strings along with thier offsets, and hex translations. 

Since the diff works in the opposite way you would expect, by which I mean it shows you all of the lines which are the same, the output
is to a csv file called diff.csv. The format is <string>, <hex>, <offset from the first file>,<offset in the second file>. 
I also think that there are serious bugs in this diff. Chaining multiple diffs are almost certainly broken.

The csv option puts the output into csv format, that is, one line per string in the order of <string>,<offset>,<hex>. Headers are printed 
into the csv file as well.


```
 ./dex_parser.py -h
usage: dex_parser.py [-h] [--diff DIFF] [--csv CSV] [--strings] file

positional arguments:
  file         First dex file you want to parse, required argument

optional arguments:
  -h, --help   show this help message and exit
  --diff DIFF  Second dex file to diff against, currently only works for
               strings
  --csv CSV    Output in csv format to this file, does not yet work for diffs
  --strings    Print out the complete strings table, includes string,offset,
               and hex

./dex_parser.py ../classes.dex --strings


String: zoomIn
Offset: 2819910
Hex String: 067a6f6f6d496e

String: zoomOut
Offset: 2819918
Hex String: 077a6f6f6d4f7574

String: zoomTo
Offset: 2819927
Hex String: 067a6f6f6d546f

String: zoom_enter
Offset: 2819935
Hex String: 0a7a6f6f6d5f656e746572

and so on...

```


I mostly wrote this to solve problems I'm having, so I haven't really focused on making the code nice, but I think it is pretty readable.
 
My immediate future goals are to add the ability to diff the string table against another dex file. -complete

There was a request for csv functionality, I've added that, but not for diffs. I don't know how to represent a diff in csv format


After that, I will like add parsing capability to the other items. Once that is done, perhaps integrating a java decompiler to help unpack the code section. I've no idea how to do that though, so it is far in the future.

### Contribution guidelines ###

If you have contributions, suggestions or anything like that, please let me know, I will respond quickly to any feedback I get.

### Who do I talk to? ###

Send me a message or pull request and I'll merge it or let you know why I didn't/won't as quickly as I can.