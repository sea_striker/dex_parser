#!/usr/bin/python

import struct, binascii, hashlib, argparse, pprint, csv
import sys, os.path, zipfile

def parse_dex(file):
  dex = parse_header(file)
  return get_strings(dex, file)
  
def parse_header(file):
  with open(file,"rb") as dex_file:
    dex_file.seek(0)
    header = dex_file.read(0x70)
    dex_file.seek(32)
    sha_check = dex_file.read(-1)
  dex_file.close()
  header_format = ('<8s4s20s20I')
  bin_struct = struct.Struct(header_format)
  header_struct = bin_struct.unpack(header)
  header_dic = {}
  header_dic['magic']          = header_struct[0]
  header_dic['adler32']        = header_struct[1]
  header_dic['sha1_signature'] = header_struct[2]
  header_dic['file_len']       = header_struct[3]
  header_dic['header_size']    = header_struct[4]
  header_dic['endian_tag']     = header_struct[5]
  header_dic['link_size']      = header_struct[6]
  header_dic['link_off']       = header_struct[7]
  header_dic['map_off']        = header_struct[8]
  header_dic['string_ids_size'] = header_struct[9]
  header_dic['string_ids_off']  = header_struct[10]
  if ( binascii.hexlify(header_dic['sha1_signature']) != hashlib.sha1(sha_check).hexdigest()):
    print "Embedded sha signature does not match actual signature"
  if ( binascii.hexlify(header_dic['magic']) != '6465780a30333500'):
    print "Header Magic is unknown, please ensure this is a dex file"
  return header_dic


def get_strings(header_dic, file):
# Returns a array of arrays of strings,
# each array contains in [0] the string itself in ascii, in [1] the offset from the beginning of the file
# and in [2] the hex data of the string ( including the size marker at the beginning, but 
# not the null bytes surrounding it )
  strings = []
  offset = header_dic['string_ids_off']
  size = header_dic['string_ids_size']
  length = header_dic['file_len']
  with open(file, "rb") as strings_dex:
    strings_dex.seek(offset)
    st = strings_dex.read(size*4)
    string_map_format = ("<" + str(size) +"I")
    sstruct = struct.Struct(string_map_format)
    string_offset_map = sstruct.unpack(st)
    for string_data_offset in string_offset_map:
      if( string_data_offset >= length ):
	print "File is likely corrupted, string data is offset beyond the end of the file" 
	break
      strings_dex.seek(string_data_offset)
      str_size_format = ("<B")
      str_size_struct = struct.Struct(str_size_format)
      str_size = strings_dex.read(1)
      try:
        str_size_unpacked = str_size_struct.unpack(str_size)
      except struct.error as exception:
        print "Unable to unpack: " + exception.args[0]
        print "Attempted integer was " + str_size + " at offset " + str(string_data_offset)
        continue
      strings_dex.seek(string_data_offset + 1)
      str_data_format = ("<"+ str(str_size_unpacked[0])+"s")
      str_data_struct = struct.Struct(str_data_format)
      str_data = strings_dex.read(str_size_unpacked[0])
      s = []
      try:
        s.append(str_data_struct.unpack(str_data)[0].encode('string_escape'))
	s.append(string_data_offset)
	s.append(binascii.hexlify( str_size + str_data ))
        strings.append(s)
      except struct.error as exception:
        print "Unable to unpack string data " + exception.args[0] 
        continue
    strings_dex.close()
  return strings

def do_diff(dex1,dex2,match):
  # We expect the hash from parsing the dex files
  # Dex1 is the first dex passed in, dex2 is passed in with the --diff argument
  # What we are trying to do on a first run is compare the hex of the strings, we will only display exact matches 
  for dex1_str in dex1:
    if ( len(dex1_str[0]) <= 2):
      continue
    hex_str = dex1_str[2]
    for dex2_str in dex2:
      if hex_str == dex2_str[2]:
        match.append([dex1_str[0],dex1_str[1],dex1_str[2]])
  return match
  # with open('diff.csv',"wb") as diff:
  #   fieldnames = ['String', 'Hex Data', 'Dex 1 Offset', 'Dex 2 Offset']
  #   writer = csv.DictWriter(diff, dialect='excel', skipinitialspace=True, lineterminator='\n', quotechar="'",fieldnames=fieldnames,quoting=csv.QUOTE_ALL)
  #   writer.writeheader()
  #   for line in match:
  #     writer.writerow({'String': line[0] , 'Hex Data': line[1] , 'Dex 1 Offset': line[2] , 'Dex 2 Offset': line[3]})
  # diff.close()

def export_to_csv(dex, csv_file):
  # We expect a hash from parsing the dex files
  with open(csv_file, "wb") as csvfile:
    fieldnames = ['String', 'Offset', 'Hex Data']
    writer = csv.DictWriter(csvfile, dialect='excel', skipinitialspace=True, lineterminator='\n', quotechar="'",fieldnames=fieldnames,quoting=csv.QUOTE_ALL)
    writer.writeheader()
    for line in dex:
      writer.writerow({'String': line[0], 'Offset': line[1], 'Hex Data': line[2]})
  csvfile.close()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("file",help="First dex file you want to parse, required argument")
  parser.add_argument("--diff",nargs="+",help="Second dex file to diff against, currently only works for strings")
  parser.add_argument("--csv",help="Output in csv format to this file, does not yet work for diffs")
  parser.add_argument("--strings",help="Print out the complete strings table, includes string,offset, and hex",action="store_true")
  args = parser.parse_args() 
  if( os.path.exists(args.file) ):
    file1 = parse_dex(args.file)
  else:
    print args.file + " does not exist or is not accessible"
  if( args.diff ):
    for f in args.diff:
      if( os.path.exists(args.diff)):
        file2 = parse_dex(args.diff)
      else:
        print args.diff + " does not exist or is not accessible"
    for f in args.diff:
      do_diff(file1, f)
  if( args.csv ):
    export_to_csv(file1,args.csv)
  if( args.strings ):
    for st in file1['string_data']:
      if ( len(st[0]) <= 2 ):
	continue
      print "String: \'%s\'" %  st[0]
      print "Offset: %d" %  st[1]
      print "Hex String: %s" %  st[2]
